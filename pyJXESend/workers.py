from threading import Thread, ThreadError
from encrypt import encryptAES
from json2xml import json2xml
from util.log import getLogger
from drivers_manager import DriverManager

LOG = getLogger(__name__)

class BasicWorker(Thread):

    def __init__(self, queue, func):
        Thread.__init__(self)
        self.queue = queue
        LOG.info('Creating Worker {}'.format(self.getName()))
        self.func = func

    def run(self):
        raise NotImplementedError()

class Worker(BasicWorker):

    def run(self):
        while True:
            args = self.queue.get()
            try:
                self.func(*args)
            except: 
                raise ThreadError('Something went wrong with thread')
            finally:
                self.queue.task_done()