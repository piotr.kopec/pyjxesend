import json
from dicttoxml import dicttoxml
import codecs

from util.log import getLogger

LOG = getLogger(__name__)

def json2xml(jsonfilename, xmlfilename=None, encoding='utf8'):
    LOG.info('json2xml {}'.format(jsonfilename))
    if '/' in jsonfilename:
        raise ValueError("'/' not allowed in filenames")
    if not xmlfilename:
        xmlfilename = jsonfilename + '.xml'

    with codecs.open(jsonfilename, mode='r', encoding=encoding) as fd:
        LOG.info('Loading {} JSON file'.format(jsonfilename))
        d = json.load(fd)
        LOG.debug('Loaded JSON dict: \n {}'.format(d))

    LOG.info('Converting dict from loaded JSON to XML file {}'.format(xmlfilename))
    xml_data = dicttoxml(d, attr_type=False)
    LOG.debug('Converted XML data: \n {}'.format(xml_data))
    
    with codecs.open(xmlfilename, mode='w') as fd:
        fd.write(xml_data)
    

