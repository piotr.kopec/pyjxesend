
from remote_drivers.gcloud import Gcloud
from config import Config
from util.log import getLogger

LOG = getLogger(__name__)



class DriverManager(object):

    DRIVERS = {'gcloud': Gcloud}

    def __init__(self):
        LOG.info('Selected {} driver: {}'.format(Config.JXE_REMOTE_BACKEND, DriverManager.DRIVERS[Config.JXE_REMOTE_BACKEND]))
        driver = DriverManager.DRIVERS[Config.JXE_REMOTE_BACKEND]
        self.driver = driver()

        LOG.info('Created driver {}'.format(self.driver))

    def upload(self, src, dest):
        self.driver.upload(src, dest)