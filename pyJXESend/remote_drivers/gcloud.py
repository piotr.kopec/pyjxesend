from config import Config
from . import Driver
from google.cloud import storage, exceptions
import os
from util.log import getLogger
import sys 

LOG = getLogger(__name__)


class Gcloud(Driver):

    def __init__(self):
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = Config.GCLOUD_DRIVER_CREDS
        LOG.info('{}'.format(os.environ["GOOGLE_APPLICATION_CREDENTIALS"]))
        try:
            self.storage_client = storage.Client.from_service_account_json(Config.GCLOUD_DRIVER_CREDS)
            LOG.info('Gcloud client created: {}'.format(self.storage_client))
        except:
            LOG.error('Unable to connect')
            raise
    
    def upload(self, filename, dest):
        self._create_bucket(dest)
        LOG.info('Created bucket {}'.format(dest))
        destinantion_blob_name = filename
        blob = self.bucket.blob(destinantion_blob_name)
        blob.upload_from_filename(filename)
        LOG.info('Uploaded file {}'.format(filename))

    def _create_bucket(self, bucket_name):
        try:
            LOG.info('Create bucket {}'.format(bucket_name))
            self.bucket = self.storage_client.create_bucket(bucket_name)
            LOG.info('Created bucket {}'.format(self.bucket))
        except exceptions.Conflict:
            try:
                self.bucket = self.storage_client.get_bucket(bucket_name)
                LOG.info('Conflict')
            except:
                LOG.error('{} is not a valid bucket name, try different one '.format(bucket_name))
                raise exceptions.Forbiden
        except:
            LOG.error('Uexpected error while creating bucket')
            raise TypeError
