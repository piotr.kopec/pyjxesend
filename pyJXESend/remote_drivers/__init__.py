from abc import ABCMeta, abstractmethod


class Driver:
    __metaclass__ = ABCMeta

    DRIVER = None
    
    @abstractmethod
    def upload(self, *args, **kwargs):
        pass
