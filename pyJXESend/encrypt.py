from Crypto.Cipher import AES
from hashlib import md5
from util.log import getLogger

LOG = getLogger(__name__)

def encryptAES(filename, key=None, outfilename=None):
    if not key:
        raise ValueError('No key provided!')
    if not outfilename:
        outfilename = filename + '.bin'
    key = _getMd5(key)
    cipher = AES.new(key, AES.MODE_EAX)
    
    with open(filename, 'rb') as f:
        data = f.read()
    
    ciphertext, tag = cipher.encrypt_and_digest(data)
    LOG.info('Encrypting file {}'.format(filename))
    with open(outfilename, 'wb') as f:

        [ f.write(x) for x in (cipher.nonce, tag, ciphertext) ]

def _getMd5(key):
    m = md5()
    m.update(key)
    key = m.hexdigest()
    return key

def _decodeAES(filename, key=None, outfilename=None):
    if not key:
        raise ValueError('No key provided!')
    if not outfilename:
        outfilename = filename + '.bin-decoded'
    key = _getMd5(key)

    with open(filename, 'rb') as f:
        nonce, tag, ciphertext = [ f.read(x) for x in (16, 16, -1) ]
    cipher = AES.new(key, AES.MODE_EAX, nonce)
    data = cipher.decrypt_and_verify(ciphertext, tag)
    print(data.decode('utf8'))
