# Solution to task:
*Note*: venv requirements in `requirements.txt` file

Usage:
```
» python pyjxesend.py --help
Usage: pyjxesend.py [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  convert-files
  encrypt-files
  send-files

» python pyjxesend.py convert-files --help
Usage: pyjxesend.py convert-files [OPTIONS] FILENAME...

Options:
  -c, --config PATH
  --help             Show this message and exit.

» python pyjxesend.py encrypt-files --help
Usage: pyjxesend.py encrypt-files [OPTIONS] FILENAME...

Options:
  -c, --config PATH
  --help             Show this message and exit.

» python pyjxesend.py send-files --help                                                                                                                               2 ↵
Usage: pyjxesend.py send-files [OPTIONS] FILENAME... [REMOTE_DEST]

Options:
  -c, --config PATH
  --help             Show this message and exit.

```
*Note*: There is only one remote driver impleneted Google Cloud Storage. In `config.py` file specify path to your GCloud authentication JSON file. For this driver REMOTE_DEST is the name of remote bucket.

### Known issues:
- only one driver: Google Cloud Storage
- configs in Python class
- no tests