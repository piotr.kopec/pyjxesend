from logging import basicConfig, getLogger, ERROR
from config import Config

FORMAT = '%(asctime)s - %(name)s - %(threadName)s - %(levelname)s - %(message)s'

basicConfig(filename='pyJXESend.log', format=FORMAT, level=Config.JXE_LOG_LEVEL)

dicttoxml_logger = getLogger('dicttoxml')   # I don't need this module to log heavily
dicttoxml_logger.setLevel(ERROR)
