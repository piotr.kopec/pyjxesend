import click
from pyJXESend import encrypt
from pyJXESend import json2xml
from pyJXESend.workers import Worker
from pyJXESend.drivers_manager import DriverManager
from Queue import Queue
from threading import Thread, ThreadError
from util.log import getLogger
from config import Config

LOG = getLogger('pyjxesend')

@click.group()
def cli():
    pass



@cli.command()
@click.argument('filename', nargs=-1, required=1)
@click.option('--config', '-c', type=click.Path(exists=True))
def encrypt_files(filename, config):
    key = Config.JXE_ENC_KEY
    n_workers = Config.JXE_MAX_THREDS
    queue = Queue()
    for _ in range(n_workers):
        worker = Worker(queue, encrypt.encryptAES)
        worker.daemon = True
        worker.start()

    for f in filename:
        LOG.info('Putting {} to queue. Key {}'.format(f, key))
        queue.put((f, key))
    queue.join()


@cli.command()
@click.argument('filename', nargs=-1, required=1)
@click.option('--config', '-c', type=click.Path(exists=True))
def convert_files(filename, config):
    n_workers = Config.JXE_MAX_THREDS
    queue = Queue()
    for _ in range(n_workers):
        worker = Worker(queue, json2xml.json2xml)
        worker.daemon = True
        worker.start()
    for f in filename:
        LOG.info('Putting {} to queue.'.format(f))
        queue.put((f,))
    queue.join()

@cli.command()
@click.argument('filename', nargs=-1, required=1)
@click.argument('remote_dest', nargs=1, required=0, default=None)
@click.option('--config', '-c', type=click.Path(exists=True))
def send_files(filename, remote_dest, config):
    if not remote_dest:
        try:
            remote_dest = Config.GCLOUD_BUCKET_NAME
        except:
            raise ValueError('No destination specified in cli or conf file')
    n_workers = Config.JXE_MAX_THREDS
    queue = Queue()

    driver = DriverManager()
    for _ in range(n_workers):  
        try:
            worker = Worker(queue, driver.upload)
            worker.daemon = True
            worker.start()
        except:
            raise ThreadError('ERROR')

    for f in filename:
        LOG.info('Putting {} to queue.'.format(f))
        queue.put((f, remote_dest))
    queue.join()


if __name__ == '__main__':
    cli()