import os
import logging
from multiprocessing import cpu_count
from util.log import getLogger

LOG = getLogger(__name__)

class Config(object):

    JXE_LOG_LEVEL = os.environ.get('JXE_LOG_LEVEL') or logging.INFO
    JXE_ENC_KEY = os.environ.get('JXE_ENC_KEY') or 'you-will-never-guess'
    JXE_MAX_THREDS = os.environ.get('JXE_MAX_THREDS') or cpu_count()
    JXE_REMOTE_BACKEND = os.environ.get('JXE_REMOTE_BACKEND') or 'gcloud'
    GCLOUD_DRIVER_CREDS = os.environ.get('GCLOUD_DRIVER_CREDS') or '/home/piotr/Desktop/personal-f94eac6bb11a.json'
    GCLOUD_BUCKET_NAME = 'neudea_test'

if Config.JXE_LOG_LEVEL == logging.DEBUG:
    LOG.debug('Running config:')
    for k, v in Config.__dict__.items():
        if not '__' in k:
            LOG.debug('{} : {}'.format(k, v))
